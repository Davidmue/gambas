#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2002-11-01 04:27+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: .project:1
msgid "Lighttable"
msgstr "-"

#: .project:2
msgid "A tool to sort photographs"
msgstr "Nástroj na třídění fotografií"

#: FHelp.form:11
msgid "Help"
msgstr "Nápověda"

#: FHelp.form:31 FInfo.form:17
msgid "&Close"
msgstr "&Zavři"

#: FInfo.class:10
msgid "There are no Exif informations in this file."
msgstr "O tomto souboru nejsou Exif informace."

#: FInfo.class:29
msgid "&Less"
msgstr "&Méně"

#: FInfo.form:11
msgid "Picture Informations"
msgstr "Informace o obrázku"

#: FInfo.form:24
msgid "&More"
msgstr "&Více"

#: FMain.class:32
msgid "Lighttable   -   "
msgstr "-"

#: FMain.class:46
msgid "Right-click for Main Menu"
msgstr "Klikni pravým pro menu"

#: FMain.class:47
msgid " pictures"
msgstr " obrázek"

#: FMain.class:48
msgid "sorted alphabetically"
msgstr "seřadit abecedně"

#: FMain.class:74
msgid "Right-click on the background or on a frame for menus."
msgstr "Klikni pravým na pozadí a nebo rám pro menu."

#: FMain.class:94
msgid "Right-click for Picture Menu"
msgstr "Klikni pravým pro menu obrázku"

#: FMain.class:173
msgid "&Rename all files..."
msgstr "&Přejmenovat všechny soubory..."

#: FMain.class:268
msgid "Loading picture..."
msgstr "Načítání obrázku..."

#: FMain.class:343
msgid "sorted chronologically"
msgstr "seřadit chronologicky"

#: FMain.class:383
msgid "The file &1 already exists in the current directory!"
msgstr "Soubor &1 již existuje v aktuální složce!"

#: FMain.class:435 FRename.form:32 FRenameAll.form:80
#: FRenameAllWarning.form:17 FStart.form:26 FTime.form:51
msgid "&Cancel"
msgstr "&Zrušit"

#: FMain.class:435 FRename.form:26 FRenameAll.form:74 FStart.form:20
#: FTime.form:45
msgid "&OK"
msgstr "-"

#: FMain.class:435
msgid "The file &1 will be deleted."
msgstr "Soubor &1 bude smazán."

#: FMain.class:727
msgid "Loading of pictures is being aborted..."
msgstr "Načítání obrázků bylo přerušeno..."

#: FMain.class:778
msgid "Files are being renamed..."
msgstr "Soubor byl přejmenován..."

#: FMain.class:807
msgid "&1 files renamed"
msgstr "Soubor &1 přejmenován"

#: FMain.class:823
msgid "Setting time informations in all files..."
msgstr "Nastavení časové informace ve všech souborech..."

#: FMain.form:44
msgid "Main menu"
msgstr "Hlavní menu"

#: FMain.form:48
msgid "Sorted &alphabetically"
msgstr "Seřadit &abecedně"

#: FMain.form:55
msgid "Sorted &chronologically"
msgstr "Seřadit &chronologicky"

#: FMain.form:61
msgid "&Slideshow"
msgstr "&Promítání"

#: FMain.form:67
msgid "Abort &Loading"
msgstr "Přerušit &načítání"

#: FMain.form:73
msgid "&Time correction..."
msgstr "&Korekce času..."

#: FMain.form:79
msgid "&Open folder..."
msgstr "&Otevřít složku..."

#: FMain.form:85
msgid "&Help"
msgstr "&Nápověda"

#: FMain.form:91
msgid "&Quit"
msgstr "&Ukončit"

#: FMain.form:98
msgid "Picture menu"
msgstr "Menu obrázku"

#: FMain.form:102
msgid "(Move picture: drag frame with mouse)"
msgstr "(Přesun obrázku: uchop s myší rám)"

#: FMain.form:107
msgid "&View picture (click on picture)"
msgstr "&Zobrazení obrázku (klikni na obrázek)"

#: FMain.form:113
msgid "&Full Screen View"
msgstr "&Plný náhled obrázku"

#: FMain.form:119
msgid "&Close view"
msgstr "&Zavřít pohled"

#: FMain.form:125
msgid "&Next picture"
msgstr "&Další obrázek"

#: FMain.form:131
msgid "&Previous picture"
msgstr "&Předchozí obrázek"

#: FMain.form:137
msgid "Picture &informations"
msgstr "&Informace o obrázku"

#: FMain.form:143
msgid "&Rename picture"
msgstr "&Přejmenovat obrázek"

#: FMain.form:149
msgid "&Delete picture"
msgstr "&Smazat obrázek"

#: FRename.class:43
msgid "Old and new filename are identical."
msgstr "Starý a nový název je identický."

#: FRename.form:11
msgid "Rename"
msgstr "Přejmenovat"

#: FRename.form:17
msgid "New filename:"
msgstr "Nový název souboru:"

#: FRenameAll.class:11
msgid "MyPicture.JPG"
msgstr "-"

#: FRenameAll.form:22
msgid "Rename all pictures"
msgstr "Přejmenovat všechy obrázky"

#: FRenameAll.form:41
msgid "1"
msgstr "-"

#: FRenameAll.form:47
msgid "Keep original filename as suffix"
msgstr "Nechat originální název jako příponu"

#: FRenameAll.form:53
msgid "Number format"
msgstr "Číselný formát"

#: FRenameAll.form:58
msgid "digits"
msgstr "číslice"

#: FRenameAll.form:63
msgid "Prefix"
msgstr "Předpona"

#: FRenameAll.form:68
msgid "Start value"
msgstr "Startovní hodnota"

#: FRenameAll.form:88
msgid "This function will rename <b>all</b> .jp(e)g files in the directory and add serial numbers to the filenames.<br>The pictures will be handled in the order they are shown right now, in rows from left to right, rows from top to bottom."
msgstr "Tato funkce přejmenuje <b>všechny</b> .jp(e)g soubory v adresáři a přidat sériová čísla do názvu souborů. <br>Fotografie budou zpracovány v pořadí, v jakém jsou právě teď zobrazeny, v řádcích zleva doprava, řádky od shora dolů."

#: FRenameAll.form:94
msgid "Options"
msgstr "Možnosti"

#: FRenameAll.form:99
msgid "Example:"
msgstr "Příklad:"

#: FRenameAllWarning.form:12
msgid "File Conflicts"
msgstr "Konflikt souboru"

#: FRenameAllWarning.form:24
msgid "Continue &anyway"
msgstr "Pokračuj &stejně"

#: FRenameAllWarning.form:29
msgid "The following files can't be renamed, because the target filenames already exist in the directory.<br>If you continue, only the files without conflicts will be renamed.<br>If you cancel, you can choose new filename options."
msgstr "Následující soubory nelze přejmenovat, protože cílový souborů již v adresáři existuje.<br>Budete-li pokračovat, budou přejmenovány pouze soubory bez konfliktů.<br>Pokud zrušíte, můžete zvolit možnost nového jména souboru."

#: FSlideshow.form:38
msgid "Pause between pictures:"
msgstr "Pauza mezi obrázky:"

#: FSlideshow.form:49
msgid "sec"
msgstr "-"

#: FSlideshow.form:54
msgid "Stop"
msgstr "-"

#: FStart.class:54
msgid "The folder &1 doesn't contain any jp(e)g files."
msgstr "Složka &1 neobsahuje žádný soubor typu jp(e)g."

#: FStart.form:15
msgid "Lighttable   -   Select picture folder"
msgstr "Lighttable   -   Výběr složky obrázků"

#: FStart.form:41
msgid "New selection"
msgstr "Nový výběr"

#: FStart.form:51
msgid "Last selections"
msgstr "Poslední výběry"

#: FTime.class:18
msgid "There is no time correction selected."
msgstr "Není vybrán čas korekce."

#: FTime.form:18
msgid "Time correction"
msgstr "Korekce času"

#: FTime.form:39
msgid "earlier"
msgstr "dříve"

#: FTime.form:39
msgid "later"
msgstr "později"

#: FTime.form:59
msgid "This function will correct all date/time informations in the EXIF section of <b> all</b> .jp(e)g files in the folder.<br>A backup of the files will be made with file names like 'MyPicture.jpg_original'.<br>If there are no EXIF date/time informations in a file, nothing will be done."
msgstr "Tato funkce opraví všechny informace o datu/času v sekci EXIF <b> všechny</ b>. jp(e)g soubory ve složce.<br>Záloha souborů s názvy souborů bude jako 'MyPicture.jpg_original'.<br>Pokud není EXIF informace o datu/času v souboru, nebude dělat nic."

#: FTime.form:65
msgid "Set the original time of all pictures to"
msgstr "Nastav původní čas všech obrázků na"

#: FTime.form:70
msgid "day(s)"
msgstr "dní"

#: FTime.form:75
msgid "hour(s)"
msgstr "hodin"

#: FTime.form:80
msgid "minute(s)"
msgstr "minut"

#: MMain.module:12
msgid "To run this program, exiftool must be installed."
msgstr "Aby program běžel, musí být nainstalován exiftool."

#: MMain.module:18
msgid "To run this program, convert must be installed."
msgstr "Aby program běžel, musí být nainstalován convert."
