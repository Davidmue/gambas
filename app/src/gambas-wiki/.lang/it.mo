��    "      ,  /   <      �     �        	                        ,     1     8     ?     T     \     d  
   m     x     }  %   �      �     �  N   �     <  %   P     v     �     �     �     �  	   �  
   �     �     �  	   �       �             	                    '     3     <     C     J  	   c     m     t  
   v     �  
   �  0   �  +   �  +   �  Y        u  ,   �     �     �     �                %     .     :     C     K  	   X                     
                                             "       	                                                !                                     Cancel Classes Constants Create Delete Description Edit Errors Events Gambas Documentation Message Methods Password Properties Save See also This class acts like a &1 / &2 array. This class acts like a &1 array. This class can be used as a &1. This class can be used like an object by creating a hidden instance on demand. This class inherits This class is &1 with the &2 keyword. This class is &1. This class is not creatable. This class is static. This class reimplements Undo creatable enumerable function read read-only write Project-Id-Version: PACKAGE VERSION
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 Annulla Classi Costanti Crea Cancella Descrizione Modifica Errori Eventi Documentazione di Gambas Messaggio Metodi - Proprietà Salva Vedi anche Questa classe si comporta come un &1 / &2 array. Questa classe si comporta come un &1 array. Questa classe può essere usata come un &1. Questa classe può essere usata come un oggetto creando un istanza nascosata a richiesta. Questa classe riceve Questa classe è &1 con la parola chiave &2. Questa classe è &1. Questa classe non è creabile. Questa classe è statica. Questa classe reimplementa Undo creabile enumerabile funzione lettura solo-lettura scrittura 