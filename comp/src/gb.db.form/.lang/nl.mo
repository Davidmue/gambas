��    1      �  C   ,      8     9     @     F     T     [     a     g     o     v     �  !   �     �     �     �  
   �     �     �     �     �     �     �                     $     )     2     :     A     F     X     `     f     k     �     �     �     �     �     �  #   �          &     2     >     J     V     b  �   n     [     b     k  	   w  	   �     �  
   �     �     �     �  #   �  
   �     �     �          
  
             (     *     <     R     Z     _     e     n     u     }     �     �  
   �     �     �      �  #   �      	  !   	      A	      b	     �	  ,   �	     �	     �	     �	     �	     �	     �	     �	                                           (              %            +   "   -         0   .      	                      
   *   $   !              )                    &            ,                       '       /         #   1           Active Birth Blob contents Cancel Clear Color Comment Create Data bound controls Delete Do you really want to clear blob? Editable End False First Name Id Image Incorrect value. Info Invalid value. Load blob from file Load... Name New Next Previous Refresh Salary Save Save blob to file Save... Start True Unable to clear blob Unable to delete record. Unable to load file Unable to save file Unable to save record. Unable to save value. Unknown You must fill all mandatory fields. Élément 1 Élément 2 Élément 3 Élément 4 Élément 5 Élément 6 Élément 7 Project-Id-Version: gb.db.form 3.10.90
PO-Revision-Date: 2017-08-26 19:37 UTC
Last-Translator: Willy Raets <gbWilly@openmailbox.org>
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 Actief Geboorte Blob inhoud Annuleren Opschonen Kleur Commentaar Creëer Data gebonden controls Verwijderen Wil je werkelijk de blob opschonen? Bewerkbaar Einde Vals Voornaam - Afbeelding Onjuiste waarde. - Ongeldige waarde. Laad blob van bestand Laad... Naam Nieuw Volgende Vorige Ververs Salaris Opslaan Blob opslaan in bestand Opslaan... Start Waar Onmogelijk om blob op te schonen Onmogelijk om record te verwijderen Onmogelijk om bestand te laden Onmogelijk om bestand op te slaan Onmogelijk om record op te slaan Onmogelijk om waarde op te slaan Onbekend Je dient alle verplichte velden in te vullen - - - - - - - 