��    &      L  5   |      P     Q     X     k  
   r     }     �     �     �     �     �     �     �     �     
  (        @     I     W     _  	   d     n     t  &   {     �     �     �     �     �     �  	   �     �     �     �  6   �     ,     B  ,   I     v  	   w     �     �     �     �      �     �     �     �  #   �       !   2     T     g  (   s     �     �  
   �  
   �     �     �  	   �     �     �       	             %     '     /     ;     C     I  2   P     �     �  "   �               	   #                                     !                       
                                                  $          %         &                       "        &Close &Close current tab &Reset &Sort tabs &Undo '&1' toolbar configuration Action Button1 Cancel Close &all other tabs Configure &1 toolbar Configure main toolbar Configure shortcuts Configure... Do you really want to reset the toolbar? Expander Find shortcut Go back Help Icon size Large Medium Multiple document interface management Next tab OK Orientation Previous tab Reparent Reset Separator Shortcut Small Space This shortcut is already used by the following action: Toolbar configuration Window You are going back to the default shortcuts. Project-Id-Version: PACKAGE VERSION
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 Za&vřít &Zavři aktuální záložku O&bnovit &Seřaď záložky &Zpět '&1' nastaveni panelu nástrojů Akce - Zrušit Zavři &všechny ostatní záložky Nastavit &1 panel nástrojů Nastavit hlavní panel nástrojů Nastavení zkratek Nastavit... Opravdu chcete obnovit panel nástrojů? - Najdi zkratku Vtátit se Nápověda Velikost ikony Velké Střední MDI rozhranní Další záložka - Orientace Předchozí záložka - Obnovit Oddělovač Zkratka Malé Mezera Zkratka se již používá na následující akci: Nastavní panelu nástrojů Okno Jděte zpět na výchozí zkratky. 