��    ;      �  O   �           	     $     +  
   >     I     P     X     _     e     t  	   �     �  	   �     �     �     �  
   �  (   �     %     .     5     F     T     k     s     x  	   }     �     �     �     �     �  &   �     �     �     �     �       
                  (     .     3  	   ;     E     N     S  	   Y     c  )   i  6   �     �      �           "     '  ,   .  �   [     I	     d	     j	     �	     �	     �	  	   �	     �	     �	     �	  	   �	     �	     �	     
     
     7
     T
  &   d
     �
  	   �
     �
     �
     �
     �
     �
  
   �
       	             4     :     A  "   C     f     s     u     x  
   �     �  	   �     �     �     �     �  	   �     �     �     �     �       1     <   ?     |  7   �  7   �            0        5   	   &          '       "              +   7   #   *      $      3       2             4         /         
                        0   :   ;       6   .   (   )                              9                                              1             ,   -         %      !   8    '&1' toolbar configuration Action Agnostic Scan Tool Attach tab Border Button1 Cancel Close Close all tabs Close other tabs Close tab Close tabs on the right Configure Configure &1 toolbar Configure main toolbar Configure shortcuts Detach tab Do you really want to reset the toolbar? Expander Export Export shortcuts Find shortcut Gambas shortcuts files Go back Help Huge Icon size Import Import shortcuts Large Medium Menu1 Multiple document interface management Next tab Numériser Texte OK Orientation Previous tab Properties Quit Reparent Reset Save Save as Separator Shortcut Show Small Sort tabs Space This file is not a Gambas shortcuts file. This shortcut is already used by the following action: Toolbar configuration Unable to export shortcut files. Unable to import shortcut files. Undo Window You are going back to the default shortcuts. Project-Id-Version: gb.form.mdi 3.10.90
PO-Revision-Date: 2017-08-26 19:41 UTC
Last-Translator: Willy Raets <gbWilly@openmailbox.org>
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 '&1' werkbalk configuratie Actie Agnostisch scan gereedschap Tab bevestigen Rand - Annuleren Sluiten Sluit alle tabs Sluit overige tabs Sluit tab Sluit tabs aan de rechterkant Configureer Configureer &1 werkbalk Configureer hoofdwerkbalk Snelkoppelingen configureren Tab loskoppelen Wil je werkelijk de werkbalk resetten? Expander Exporteer Exporteer snelkoppeling Vind snelkoppeling Gambas snelkoppeling bestanden Ga terug Help Reusachtig Afmeting icoon Importeer Importeer snelkoppelingen Groot Middel - Multiple document interface beheer Volgende tab - OK Oriëntatie Vorige tab Eigenschappen Afsluiten Reparent Opnieuw instellen Opslaan Opslaan als Scheiding Snelkoppeling Toon Klein Sorteer tabs Spatie Dit bestand is geen Gambas snelkoppeling bestand. Deze snelkoppeling is reeds gebruikt door de volgende actie: Werkbalk configuratie Niet in staat om snelkoppeling bestanden te exporteren. Niet in staat om snelkoppeling bestanden te importeren. Ongedaan maken Venster Je gaat terug naar de standaard snelkoppelingen. 