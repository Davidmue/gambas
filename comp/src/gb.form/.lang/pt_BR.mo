��    +      t  ;   �      �     �     �     �     �     �     �        	                  /     H     N  !   V  +   x     �     �     �     �     �  #   �     �     �       
     
        "     %     6     ;     J     L     S  
   U     `     e  	   n     x  >        �  	   �     �     �  "   �     �  	   	               +     -  	   5     ?  &   H  )   o     �     �  #   �  -   �               )     +     -     5     7     @     V  
   [     f     t     w     �     �     �     �     �     �     �  	   �     �     �  ?   �     	  
   	     *	     '      %                            !                           )   #                                 &         $                                    	      (      
           "                    *   +    &Bookmark current directory &Edit bookmarks... &Next A All files (*) B Bold Bookmarks Cancel Cannot create directory. Cannot rename directory. Close Desktop Do not display this message again Do you really want to remove this bookmark? Edit bookmarks Follow color grid G H Home How quickly daft jumping zebras vex Italic Last modified Name New folder Next month OK Parent directory Path Previous month R Remove S Show files Size Step #&1 Strikeout System The '/' character is forbidden inside file or directory names. Today Underline V Project-Id-Version: PACKAGE VERSION
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 &Diretório corrente como favorito &Editar favoritos... &Próximo - Todos os arquivos (*) - Negrito Favoritos Cancelar Não foi possível criar o diretório. Não foi possível renomear o diretório. Fechar Área de trabalho Não exibir essa mensagem novamente Você realmente deseja excluir esse favorito? Editar favoritos Seguir cor da grade - - Início - Itálico Última modificação Nome Nova pasta Próximo mês OK Diretório pai Caminho Mês anterior - Remover - Exibir arquivos Tamanho Passo #&1 Tachado Sistema O caractere '/' é proibido em nome de arquivos ou diretórios. Hoje Sublinhado - 